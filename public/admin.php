<?php

/**
 * Задача 6. Реализовать вход администратора с использованием
 * HTTP-авторизации для просмотра и удаления результатов.
 **/

// Пример HTTP-аутентификации.
// PHP хранит логин и пароль в суперглобальном массиве $_SERVER.
// Подробнее см. стр. 26 и 99 в учебном пособии Веб-программирование и веб-сервисы.
$user = 'u20943';
$pass = '6118997';
$db = new PDO('mysql:host=localhost;dbname=u20943', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
$stmt = $db->prepare("SELECT * FROM admin_data");
$stmt->execute();
$admin_data = $stmt ->fetch();
$flag=0;
if(($admin_data['admin_id']==$_SERVER['PHP_AUTH_USER'] && $admin_data['pass']==$_SERVER['PHP_AUTH_PW'])){
  // Если все ок, то авторизуем пользователя.
  $flag=1;
  // Делаем перенаправление.
  // header('Location: ./');
}
if($flag==0){
    header('HTTP/1.1 401 Unanthorized');
    header('WWW-Authenticate: Basic realm="web_6"');
    print('<h1>401 Требуется авторизация</h1>');
    exit();
}
print('Вы успешно авторизовались и видите защищенные паролем данные.');
?>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Панель администратора</title>
    <style>
    table {
      font-family: arial, sans-serif;
      border-collapse: collapse;
      width: 100%;
    }

    td, th {
      border: 1px solid #dddddd;
      text-align: left;
      padding: 8px;
    }

    tr:nth-child(even) {
      background-color: #dddddd;
    }
    </style>
</head>
<body>
<form action="delete.php" method="post" accept-charset="UTF-8">
    <label>
        <input type="number" name="delete">
    </label>
    <input type="submit" style="margin-bottom : -1em"  class="buttonform" value="Удалить запись по ID">
</form>
<form action="change.php" method="post" accept-charset="UTF-8">
    <label>
        <input type="number" name="change">
    </label>
    <input type="submit" style="margin-bottom : -1em"  class="buttonform" value="Изменить запись по ID">
</form>
<?php

$stmt = $db->prepare("SELECT * from human_data JOIN app on human_data.id_human=app.id_user join login_n on human_data.id_human=login_n.user_id;");
$stmt->execute();
print '<table>';
print '<tr><th>user_id</th><th>fio</th><th>e-mail</th><th>birthday</th><th>gender</th><th>limb count</th>
    <th>biography</th><th>Логин</th><th>Хэш пароля</th><th>Способность</th></tr>';
$cur_id='';
while($row = $stmt ->fetch(PDO::FETCH_ASSOC)){
    if($cur_id!=$row["id_human"]){
    $cur_id=$row["id_human"];
    print '<tr><td>';
    print $row["id_human"];
    print '</td><td>';
    print $row["names"];
    print '</td><td>';
    print $row["e_mail"];
    print '</td><td>';
    print $row["hb"];
    print '</td><td>';
    print $row["gend"];
    print '</td><td>';
    print $row["limbs"];
    print '</td><td>';
    print $row["bio"];
    print '</td><td>';
    print $row["login_id"];
    print '</td><td>';
    print $row["pass"];
    print '</td><td>';
  }
    switch ($row["ab"]){
      case 0: print 'бессмертие';
        break;
      case 1: print 'прохождение сквозь стены';
        break;
      case 2: print 'левитация';
    }
    print '<br>';
    if($cur_id!=$row["id_human"]){
    print '</td></tr>';
  }
}
print '</table>';

$request = "SELECT COUNT(ab) FROM app where ab='1'";
$result = $db ->prepare($request);
$result->execute();
$count_abil1 = $result->fetch()[0];
$request = "SELECT COUNT(ab) FROM app where ab='2'";
$result = $db ->prepare($request);
$result->execute();
$count_abil2 = $result->fetch()[0];
$request = "SELECT COUNT(ab) FROM app where ab='3'";
$result = $db ->prepare($request);
$result->execute();
$count_abil3 = $result->fetch()[0];
print '<h2>Статистика по способностям:</h2>';
print '<table class="table">';
print '<tr><th>,бессмертие</th><th>прохождение сквозь стены</th><th>левитация</th></tr>';
print '<tr><td>';
print $count_abil1;
print '</td><td>';
print $count_abil2;
print '</td><td>';
print $count_abil3;
print '</td></tr>';
print '</table>';

print '<a href="./">form</a>';
?>
</body>
<html>
<?php
// *********
// Здесь нужно прочитать отправленные ранее пользователями данные и вывести в таблицу.
// Реализовать просмотр и удаление всех данных.
// *********
?>
